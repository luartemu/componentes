// import 'package:componentes/src/pages/home_temp.dart';
import 'package:componentes/src/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:componentes/src/pages/alert_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false ,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],supportedLocales: [
        const Locale('en', ''), // English, no country code
        const Locale('es', 'ES'), // 
        const Locale.fromSubtags(languageCode: 'zh'), // Chinese *See Advanced Locales below*
            // ... other locales the app supports
        ],
      title: 'Material App',
      // home:HomePage(),
      initialRoute: '/', // Configurar ruta inicial
      //Configurar las Rutas del proyecto
      routes: getAplicationRoutes()
      ,onGenerateRoute: (RouteSettings settings){
          // Es para una ruta por defecto
        return MaterialPageRoute(
          builder: (BuildContext context) =>AlertPage() ,
        );

      },
    );
  }
}