import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  // const HomePageTemp({Key key}) : super(key: key);

  final _opciones = ['Uno' , 'Dos' , 'Tres' , 'Cuatro' ,' Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp'),
      ),
        body: ListView(
          children: _crearItemsCorta()
        )
    );
  }

  List<Widget> _crearItems(){
    
    List<Widget> lista =  List<Widget>();

    for (String opt in _opciones) {
      
      final tempWidget = ListTile(
        title: Text(opt),
      );

      lista..add(tempWidget)      
          ..add(Divider());      
    }

    return lista;
  }

  List<Widget> _crearItemsCorta(){

    return _opciones.map(( item){

      return Column(
        children: <Widget>[
          ListTile(
            title: Text(item),
            leading: Icon(Icons.ac_unit),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: (){},
          ),
          Divider()
        ],
      );

    }).toList();

 

  }
}