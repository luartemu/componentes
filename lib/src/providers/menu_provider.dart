import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle; // el rootBundle es para leer archivos JSON
class _MenuProvider {

  List<dynamic> opciones = [];

  _MenuProvider(){
    cargarData();
  }

   Future<List<dynamic>> cargarData() async {
     // la siguiente linea regresa un future
     final resp = await rootBundle.loadString('data/menu_opts.json');

      // convertir un json en un objeto Map en Dart
        Map dataMap =  json.decode(resp);
        opciones = dataMap['rutas'];

        return opciones;



   }
}

final menuProvider = new _MenuProvider();





